package main.java.fr.cnam.foad.nfa035.badges.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * Renderer spécifique aux cellules contenant la taille des badges
 */
public class BadgeSizeCellRenderer extends DefaultTableCellRenderer {

    public Color originBackground;
    public Color originForeground;
    BadgeSizeCellRenderer() {
    }

    /**
     * {@inheritDoc}
     * @param table  the <code>JTable</code>
     * @param value  the value to assign to the cell at
     *                  <code>[row, column]</code>
     * @param isSelected true if cell is selected
     * @param hasFocus true if cell has focus
     * @param row  the row of the cell to render
     * @param column the column of the cell to render
     * @return
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        originBackground = table.getBackground();
        originForeground = table.getForeground();
        long size = (long) value;
        setText(humanReadableByteCountBin(size));

        if (size > 10240) {
            setBackground(Color.ORANGE);
        } else {
            setBackground(originBackground);
        }
        return this;
    }

    /**
     * Méthode qui adapte l'échelle de l'affichage de la taille du badge
     * @param bytes
     * @return Taille du badge formatée
     */
    private String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " o";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %co", value / 1024.0, ci.current());
    }
}
