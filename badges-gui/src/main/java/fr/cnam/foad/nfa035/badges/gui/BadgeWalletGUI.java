package main.java.fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class BadgeWalletGUI {
    private JPanel panelMain;
    private JButton button1;
    private JTable table1;

    /**
     * Instance de notre GUI
     */
    public BadgeWalletGUI() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Bien joué!");
            }
        });

        DirectAccessBadgeWalletDAO dao = null;
        final String RESOURCES_PATH = "./badges-wallet/src/test/resources/";

        try {
            dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            Set<DigitalBadge> metaSet = dao.getWalletMetadata();
            List<DigitalBadge> tableList = new ArrayList<>();
            tableList.addAll(metaSet);
            table1.setModel(new BadgesModel(tableList));
            table1.setAutoCreateRowSorter(true);
            table1.setDefaultRenderer(Integer.class, new DefaultBadgeCellRenderer());
            table1.setDefaultRenderer(String.class, new DefaultBadgeCellRenderer());
            table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());
            table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * Méthode principale
     * @param args
     */
    public static void main(String args[]) {
        JFrame frame = new JFrame("Badge Waller GUI");
        frame.setContentPane(new BadgeWalletGUI().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
