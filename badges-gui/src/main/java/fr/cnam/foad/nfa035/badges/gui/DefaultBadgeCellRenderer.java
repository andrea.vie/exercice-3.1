package main.java.fr.cnam.foad.nfa035.badges.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Renderer par défaut des cellules
 */
public class DefaultBadgeCellRenderer extends DefaultTableCellRenderer {
    public Color originForeground;
    DefaultBadgeCellRenderer() {
    }

    /**
     * {@inheritDoc}
     * @param table  the <code>JTable</code>
     * @param value  the value to assign to the cell at
     *                  <code>[row, column]</code>
     * @param isSelected true if cell is selected
     * @param hasFocus true if cell has focus
     * @param row  the row of the cell to render
     * @param column the column of the cell to render
     * @return
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        originForeground = table.getForeground();
        Date endDate = (Date)table.getModel().getValueAt(row, 3);

        if (new java.util.Date().compareTo(endDate) > 0) {
            setForeground(Color.RED);
        } else {
            setForeground(originForeground);
        }
        return this;
    }
}
