package main.java.fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

public class BadgesModel extends AbstractTableModel {
    private List<DigitalBadge> badges;
    private final String[] entetes = {"ID", "Code Série", "Début", "Fin", "Taille (octets)"};
    private long serialVersionUID;

    /**
     * Contructeur
     */
    public BadgesModel(List<DigitalBadge> listebadges) {
        this.badges = listebadges;
    }

    /**
     * Méthode pour récuperer les badges
     * @return Liste des badges
     */
    public List<DigitalBadge> getBadges() {
        return badges;
    }

    /**
     * {@inheritDoc}
     * @return Nombre de lignes
     */
    @Override
    public int getRowCount() {
        return badges.size();
    }
    /**
     * {@inheritDoc}
     * @return Nombre de colonnes
     */
    @Override
    public int getColumnCount() {
        return entetes.length;
    }

    /**
     * {@inheritDoc}
     * @param columnIndex  the column being queried
     * @return Classe de la colonne
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        //"ID", "Code Série", "Début", "Fin", "Taille (octets)"
        switch (columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;
            case 2:
                return Date.class;
            case 3:
                return Date.class;
            case 4:
                return Long.class;
            default:
                return Object.class;
        }
    }

    /**
     * Méthode pour obtenir le nom de la colonne
     * @param columnIndex  the column being queried
     * @return Nom de la colonne
     */
    @Override
    public String getColumnName(int columnIndex) {
        return entetes[columnIndex];
    }

    /**
     * Méthode pour récupérer la valeur d'une cellule
     * @param rowIndex        the row whose value is to be queried
     * @param columnIndex     the column whose value is to be queried
     * @return La valeur de la cellule
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        //"ID", "Code Série", "Début", "Fin", "Taille (octets)"
        switch (columnIndex) {
            case 0:
                return badges.get(rowIndex).getMetadata().getBadgeId();
            case 1:
                return badges.get(rowIndex).getSerial();
            case 2:
                return badges.get(rowIndex).getBegin();
            case 3:
                return badges.get(rowIndex).getEnd();
            case 4:
                return badges.get(rowIndex).getMetadata().getImageSize();
            default:
                throw new IllegalArgumentException();
        }
    }
}
